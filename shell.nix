{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, base, blaze-html, bytestring, containers
      , data-default-class, directory, filepath, hakyll, hakyll-sass
      , language-javascript, lens, pandoc, pandoc-types, pretty-terminal
      , regex-applicative, skylighting, skylighting-core, split, stdenv
      , tagsoup, text, time, xml
      }:
      mkDerivation {
        pname = "esrevinu-space-blog";
        version = "0.3.0.0";
        src = ./.;
        isLibrary = false;
        isExecutable = true;
        executableHaskellDepends = [
          base blaze-html bytestring containers data-default-class directory
          filepath hakyll hakyll-sass language-javascript lens pandoc
          pandoc-types pretty-terminal regex-applicative skylighting
          skylighting-core split tagsoup text time xml
        ];
        description = "Personal blog site";
        license = stdenv.lib.licenses.asl20;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
