{-|
Module      : Site.Meta
Description : Provides additional info about build.
Copyright   : (c) 2019 Vaclav Svejcar
Copyright   : (c) 2020 esrevinu.space

Stability   : experimental
Portability : portable

Module providing functions for obtaining additional meta info about the build
itself.
-}
module Site.Meta
  ( buildVersion
  )
where

import           Data.Version                   ( showVersion )
import           Paths_esrevinu_space_blog      ( version )

-- | Returns build version (specified in /package.yaml/).
buildVersion :: String
buildVersion = showVersion version
