---
title: 나와 이 블로그에 대해서
description: 나와 이 블로그를 소개
---

이 블로그는 처음으로 아마존 웹 서비스를 이용해 보면서 [Hakyll]이란 정적 웹사이트
생성기를 이용해서 만들었습니다. 소스코드는 [Vaclav Svejcar]란 분의 소스코드를
바탕으로 했으며 조금 수정을 가했습니다. 저의 소스코드는 [gitlab]에 있습니다.

이 블로그의 목적은 딱히 없으며 그냥 아무 얘기나 할 것입니다. 저의 관심사는
Haskell과 같은 프로그래밍 언어나 리눅스 사용, 그리고 물리학입니다. 어떤 것에도
전문적인 지식을 갖고 있지 않으며 그냥 취미 수준입니다.

[Hakyll]: https://jaspervdj.be/hakyll/
[Vaclav Svejcar]: https://svejcar.dev/
[gitlab]: https://gitlab.com/esrevinu/esrevinu-space-blog
