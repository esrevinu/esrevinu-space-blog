{-|
Module      : Site.Core
Description : Core functionality for the site.
Copyright   : (c) 2019 Vaclav Svejcar

Stability   : experimental
Portability : portable

Module providing functions required to properly configure Hakyll for this site.
-}
{-# LANGUAGE OverloadedStrings #-}
module Site.Core
  ( applyImageContext
  , deIndexURLs
  , directorizeDate
  , dropMore
  , postList
  , postsPattern
  , runSite
  , stripAssets
  , stripContent
  , stripStatic
  , stripIndex
  , (+||+)
  )
where

import           Control.Lens            hiding ( Context )
import           Control.Monad                  ( when )
import           Data.List                      ( intersperse
                                                , isPrefixOf
                                                , isSuffixOf
                                                )
import           Data.List.Split                ( splitOn )
import           Data.Maybe
import           Hakyll                  hiding ( tagCloudField
                                                , renderTags
                                                , match
                                                )
import           Site.Config
import           Site.Contexts                  ( postCtx )
import           Site.Types                     ( RenderMode(..) )
import           System.Console.Pretty          ( Color(..)
                                                , color
                                                )
import           System.Environment             ( getArgs
                                                , withArgs
                                                )
import           System.FilePath                ( splitExtension )
import           Text.HTML.TagSoup
import           Text.Regex.Applicative

-- | Cleans up generated files used for Draft mode.
cleanDrafts :: IO ()
cleanDrafts = do remove "_draft"
                 remove "_draft_cache"
  where
    remove dir = do putStrLn $ "Removing " ++ dir ++ "..."
                    removeDirectory dir

deIndexURLs :: Item String -> Compiler (Item String)
deIndexURLs item = return $ fmap (withUrls stripIndex) item

applyImageContext :: Context String -> Item String -> Compiler (Item String)
applyImageContext ctx item = do
  StringField imgdir <- unContext ctx "imgdir" [] =<< makeItem ""
  let repl s = if "$imgdir$" `isPrefixOf` s
               then imgdir ++ drop 8 s
               else s
  return $ fmap (withUrls repl) item

directorizeDate :: Routes
directorizeDate = customRoute (directorize . toFilePath)
  where
    directorize path =
      let (dirs, ext) = splitExtension
                        $  concat
                        $  intersperse "/" date
                        ++ ["/"]
                        ++ intersperse "-" rest
          (date, rest) = splitAt 3 $ splitOn "-" path
      in dirs ++ "/index" ++ ext

postList
  :: Pattern
  -> SiteConfig
  -> Tags
  -> ([Item String] -> Compiler [Item String])
  -> Compiler String
postList postsPattern' config tags sortFilter = do
  posts   <- sortFilter =<< loadAll postsPattern'
  itemTpl <- loadBody "templates/post-link.html"
  applyTemplateList itemTpl (postCtx config tags) posts

dropMore :: Item String -> Item String
dropMore = fmap (renderTags . dropTags . parseTags)
  where
    dropTags ts =
      let len = length ts
          tryCommentTag = takeWhile (~/= ("<!-- MORE -->" :: String)) ts
          h1 = takeWhile (not . isTagOpenName "p") ts
          ps = partitions (isTagOpenName "p") ts
      in if length tryCommentTag /= len
         then tryCommentTag
         else h1 ++ takeParagraphs ps 500 []
    takeParagraphs (p:ps) nc res =
      let text = innerText p
          len = length text
      in if length text < nc
         then takeParagraphs ps (nc - len) (res ++ p)
         else res ++ crawl p nc [] []
    crawl (t:ts) nc res close = case t of
      TagOpen name _ -> crawl ts nc (res ++ [t]) (TagClose name : close)
      TagClose name  -> crawl ts nc (res ++ [t]) (tail close)
      TagText str    -> if length str < nc
                        then crawl ts (nc - length str) (res ++ [t]) close
                        else res ++ TagText (take nc str ++ " ")
                             : parseTags "<i class=\"fas fa-ellipsis-h\"></i>"
                             ++ close
      _              -> crawl ts nc (res ++ [t]) close

postsPattern :: RenderMode -> Pattern
postsPattern Draft = "content/posts/*" .||. "content/drafts/*"
postsPattern Prod  = "content/posts/*"

runSite :: (RenderMode -> Rules ()) -> IO ()
runSite rules = do
  args <- getArgs
  let draftMode  = length args == 2 && args !! 1 == "draft"
      action     = args ^? element 0
      hakyllConf = if draftMode
        then defaultConfiguration { destinationDirectory = "_draft"
                                  , storeDirectory       = "_draft_cache"
                                  , tmpDirectory         = "_draft_cache/tmp"
                                  }
        else defaultConfiguration
      mode  = if draftMode then Draft else Prod
      args' = take 1 args

  case action of
    Just "clean" -> cleanDrafts
    _            -> return ()

  when draftMode $ putStrLn (color Yellow "🚧 RUNNING IN DRAFT MODE 🚧")
  withArgs args' $ hakyllWith hakyllConf (rules mode)

stripAssets :: Routes
stripAssets = stripRoute "assets/"

stripContent :: Routes
stripContent = stripRoute "content/"

stripRoute :: String -> Routes
stripRoute ptrn = gsubRoute ptrn $ const ""

stripStatic :: Routes
stripStatic = stripRoute "static/"

-- | Strips "index.html" from given URL string.
stripIndex :: String -> String
stripIndex str = fromMaybe str $
                 match ((\a b c -> a ++ b : c) <$> many anySym
                                               <*> sym '/'
                                               <*  string "index.html"
                                               <*> (concat <$> many query)) str
  where
    query = (:) <$> (sym '#' <|> sym '?') <*> many (psym (/= '/'))

-- | Infix version of 'composeRoutes'.
(+||+) :: Routes -> Routes -> Routes
(+||+) = composeRoutes
