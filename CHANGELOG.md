# Revision history for esrevinu-space-blog

## 0.4.0.0 -- 2020-02-28

* Modified stripIndex to support link anchor in URL
* Use `$imgdir$` for per-post image directory
* Use my fontawesome kit

## 0.3.0.0 -- 2020-02-02

* Fix tableOfContents and disqus comment
* Translate site text in Korean
* Use my own GAID
* Modified the generation of post teaser: `<!-- MORE -->` is not needed

## 0.2.0.0 -- 2020-01-28

* Adopted Vaclav Svejcar's theme
* Modified some source code to build in my environment. GHC 8.8.1
* Modified asset files to fit my blog

## 0.1.0.0 -- 2020-01-19

* First version. Released on an unsuspecting world.
