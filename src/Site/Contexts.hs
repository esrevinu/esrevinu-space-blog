{-|
Module      : Site.Contexts
Description : Collection of common Hakyll contexts.
Copyright   : (c) 2019 Vaclav Svejcar

Stability   : experimental
Portability : portable

Module providing functions for constructing Hakyll contexts.
-}
module Site.Contexts
  ( postCtx
  , siteCtx
  , imgDirCtx
  )
where

import           Data.Char                      ( isDigit )
import           Data.Maybe                     ( fromMaybe )
import qualified Data.Text                     as T
import           Hakyll                  hiding ( tagCloudField
                                                , match
                                                )
import           Site.Config
import           Site.Slug                      ( slugify
                                                , slugify'
                                                )
import           Site.Tags                      ( tagCloudField
                                                , tagLinks
                                                )
import           Text.Regex.Applicative

-- | Creates a 'Context' for blog posts.
postCtx :: SiteConfig -> Tags -> Context String
postCtx config tags = mconcat
  [ dateField "date"     "%Y년 %-m월 %-d일"
  , dateField "datetime" "%Y-%m-%d"
  , tagLinks getTags "tags" tags
  , siteCtx config tags
  ]

-- | Creates a 'Context' for main template.
siteCtx :: SiteConfig -> Tags -> Context String
siteCtx config tags = mconcat
  [ tagCloudField "cloud" 60 150 tags
  , maybeField "gaId" (scGaId config)
  , pageIdField "pageId"
  , defaultContext
  ]

maybeField :: String -> Maybe String -> Context a
maybeField key = maybe mempty (constField key)

pageIdField :: String -> Context a
pageIdField = mapContext (T.unpack . slugify . T.pack) . titleField

pageIdField' :: String -> Context a
pageIdField' = mapContext (T.unpack . slugify' . T.pack) . titleField

imgDirCtx = mapContext imgDirReplace $ pageIdField' "imgdir"
  where
    dash = sym '-'
    digit = psym isDigit
    digit2 = digit <* digit
    dateYear = digit2 <* digit2
    dateMonth = digit2
    dateDay = digit2
    dateRe = dateYear <* dash <* dateMonth <* dash <* dateDay <* dash
    imgDirReplace str = fromMaybe str $
                        match (("/images/" ++)
                                  <$> (dateRe *> many anySym)) str

