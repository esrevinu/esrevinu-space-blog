---
title: KDE connect와 GSConnect
description: GSConnect 소개
tags: 리눅스, 그놈, GSConnect, KDE connect
---

![PC에서 Mobile settings 열면 나오는 창.]($imgdir$/gsconnect-screenshot.png){ width="50%" }

최근에 [GSConnect](https://extensions.gnome.org/extension/1319/gsconnect/)라는
그놈셸 확장을 깔아 봤다. 매우 잘 작동하고 유용하다.
[https://extensions.gnome.org](https://extensions.gnome.org)에서도 인기가 좋은
것 같다. 잠깐 사이에 인기 순위가 껑충 뛰었다.

GSConnect는 KDE connect의 구현이라고 한다. KDE에서 스마트폰과 동기화하는
프로그램인데 그걸 그놈셸에서 이용할 수 있게 구현했다고 한다. 스마트폰에서는
여전히 [KDE
connect](https://play.google.com/store/apps/details?id=org.kde.kdeconnect_tp)라는
앱을 이용하면 된다.

브라우저 확장기능으로 GSConnect라는 것이 있는데 현재 보고 있는 웹페이지를
스마트폰에서 열게 하거나 SMS로 보낼 때 쓸 수 있다.

GSConnect를 설치하면 패널 오른쪽 드롭다운 메뉴에서 Mobile Devices 메뉴가 생긴다.
거기서 Mobile Settings에 들어가면 같은 네트워크 상의 KDE connect 기기를 찾는다.
당연히 스마트폰에 KDE connect 앱을 깔아야 보인다. 그리고 페어링을 하고 PC와
스마트폰에서 서로에 대해 권한을 허용해 주면 된다.

스마트폰의 알림을 PC에서 볼 수도 있고 PC의 알림을 스마트폰에서 볼 수도 있다.
후자는 켜니까 스마트폰이 너무 많이 울린다.

KDE connect 앱에는 키보드 앱이 포함되어 있는데 그 키보드 앱을 입력기로 선택하고
PC에서 Keyboard 항목을 선택해서 입력하면 스마트폰에 글자가 입력된다. 그런데
한글은 안 된다. 반대 방향으로 스마트폰에서 PC로 원격입력을 할 수 있다. 이 때는
마우스 커서도 움직일 수 있다. 꼭 터치패드처럼. 여기서도 한글 입력이 되지 않는다.

문자메시지도 볼 수 있고 스마트폰 스토리지를 마운트할 수 있다. 노틸러스를 열어
보면 SFTP로 마운트되어 있다. 그리고 서로 파일을 보내서 공유할 수 있다.
Ring이라고 있는데 스마트폰이 어디 있는지 찾기 위해서 있는 것 같다. 함부로 누르면
안 된다. Photo는 누르니까 스마트폰에 카메라앱이 열린다. 양쪽으로 미디어를
재생하고 있으면 무엇을 듣고 있는지 보여 주고 재생, 일시정지 같은 제어를 할 수
있다. 그리고 PC에 명령을 등록해서 스마트폰에서 PC가 명령을 실행하게 할 수 있다.
프리젠테이션 할 때 쓸 수 있는 슬라이드 쇼 리모콘도 있다.

또 유용한 것은 클립보드 공유가 되는 것이다. 키보드에서 한글 입력은 안 되지만
클립보드에 있는 한글은 잘 붙여 넣어진다. 그리고 스마트폰 OTP 앱에서 숫자를
복사하고 PC에서 붙여 넣으면 되니까 OTP 입력할 때 편하다.
