# esrevinu.space

This is the source code for my personal website https://esrevinu.space. It's built using [Hakyll](https://jaspervdj.be/hakyll/) static site generator, [Haskell](https://www.haskell.org)-based alternative to [Jekyll](https://jekyllrb.com).

The source code is based on [one](https://github.com/vaclavsvejcar/svejcar-dev)
for [Vaclav Svejcar's blog](https://svejcar.dev).

# Sites

This blog is served using AWS and GitLab pages.

* AWS
  - https://esrevinu.space
  - https://www.esrevinu.space
* GitLab pages
  - https://blog.esrevinu.space
  - https://esrevinu.gitlab.io/esrevinu-space-blog
